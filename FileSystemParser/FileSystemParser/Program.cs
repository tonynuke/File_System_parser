﻿using FileSystemParser.models;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileSystemParser
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] paths = { @"C:\Users\Anton\AppData\Local", @"D:\Photoshop.CC.2015" };

            var fileParser = new FileParser();

            fileParser.DirSearch(paths[0]);
            //////fileParser.SendQueueToDb();
            ////fileParser.SendQueueToDbParallel();
            fileParser.SendQueueToDbThreadPool(4);

            //var f = new FileHash("test file");

            Console.WriteLine("Files in DB");
            //FileParser.Print();

            Console.ReadLine();
        }
    }
}
