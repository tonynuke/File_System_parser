﻿using NHibernate;
using NHibernate.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace FileSystemParser.models
{
    public class FileParser
    {
        private Queue<Tuple<string, string>> queue;
        private object threadLock = new object();
        private static ISession session = null;

        public FileParser()
        {
            InitConnection();

            queue = new Queue<Tuple<string, string>>();
        }

        /// <summary>
        /// Init DB connection
        /// </summary>
        private void InitConnection()
        {
            if (session == null)
            {
                session = FluentNHibernateHelper.OpenSession();
            }
        }

        /// <summary>
        /// Close DB connection
        /// </summary>
        private void CloseConnection()
        {
            if (session != null)
            {
                session.Close();
                session.Dispose();
            }
        }

        /// <summary>
        /// DB insert using casual style
        /// </summary>
        public void SendQueueToDb()
        {
            var count = queue.Count;

            Console.WriteLine("Файлов для обработки " + count);

            //while queue is not empty
            while (queue.Count != 0)
            {
                var q = queue.Dequeue();

                session.SaveOrUpdate(new FileHash { Name = q.Item1, Hash = q.Item1.GetHashCode(), Status = q.Item2 });

                Console.WriteLine("Обработано " + "{0:0.###}", GetStatus(count, queue.Count));
            }

            Console.WriteLine("Обработка завершена");
        }

        /// <summary>
        /// DB insert using own thread for each insert
        /// </summary>
        public void SendQueueToDbParallel()
        {
            var count = queue.Count;

            Console.WriteLine("Файлов для обработки " + count);

            //// parallel for
            //Parallel.ForEach(queue, x => {
            //    session.SaveOrUpdate(new FileHash { Name = x, Hash = x.GetHashCode() });
            //});

            while (queue.Count != 0)
            {
                var thread = new Thread(
                    () =>
                    {
                        lock (threadLock)
                        {
                            if (queue.Count != 0)
                            {
                                var q = queue.Dequeue();
                                session.SaveOrUpdate(new FileHash { Name = q.Item1, Hash = q.Item1.GetHashCode(), Status = q.Item2 });
                            }
                        }
                    }
                    );
                thread.Start();

                Console.WriteLine("Обработано " + "{0:0.###} %", GetStatus(count, queue.Count));
            }

            Console.WriteLine("Обработка завершена");
        }

        /// <summary>
        /// DB insert using poolCount threads
        /// </summary>
        /// <param name="poolCount">pool count</param>
        public void SendQueueToDbThreadPool(int poolCount = 1)
        {
            for(int i = 0; i < poolCount; i++)
            {
                Thread t = new Thread(GetQueueReqursively);
                t.Start();
            }
        }

        /// <summary>
        /// DB insert reqursively
        /// </summary>
        public void GetQueueReqursively()
        {
            var count = queue.Count;

            lock (threadLock)
            {
                if (queue.Count != 0)
                {
                    var q = queue.Dequeue();
                    session.SaveOrUpdate(new FileHash { Name = q.Item1, Hash = q.Item1.GetHashCode(), Status = q.Item2 });

                    Console.WriteLine("Обработано " + "{0:0.###} %", GetStatus(count, queue.Count));

                    GetQueueReqursively();
                }
                else
                {
                    Console.WriteLine("Обработка завершена");
                    return;
                }
            }
        }

        /// <summary>
        /// Get status of db loading
        /// </summary>
        /// <param name="countAll">count of all records</param>
        /// /// <param name="currentCount">count of current records</param>
        private double GetStatus(int countAll, int currentCount)
        {
            return (double)(countAll - currentCount) / countAll;
        }

        /// <summary>
        /// Print database content
        /// </summary>
        public static void Print()
        {
            using (var session = FluentNHibernateHelper.OpenSession())
            {
                var fileHashs = session.Query<FileHash>().ToList();

                fileHashs.ForEach(x => Console.WriteLine(x.Id + " " + x.Name + " " + x.Hash + Environment.NewLine));
            }
        }

        /// <summary>
        /// Add file hash object to database
        /// </summary>
        /// <param name="name">db record name</param>
        /// <param name="hash">db record hash</param>
        public static void AddHashFile(string name, int hash)
        {
            using (var session = FluentNHibernateHelper.OpenSession())
            {
                session.SaveOrUpdate(new FileHash { Name = name, Hash = hash });
            }
        }

        /// <summary>
        /// Get all files in directory and put them into queue
        /// </summary>
        /// <param name="dir_path">Directory path to calculate</param>
        public void DirSearch(string dir_path)
        {
            try
            {
                //get all files of current dirrectory
                foreach (string file_name in Directory.GetFiles(dir_path))
                {
                    lock (threadLock)
                    {
                        queue.Enqueue(new Tuple<string, string>(file_name, ""));
                    }
                }

                //recursively search files in child dirs
                foreach (string dirrectory_name in Directory.GetDirectories(dir_path))
                {
                    DirSearch(dirrectory_name);
                }
            }
            catch (System.Exception excpt)
            {
                var status_message = excpt.Message;

                queue.Enqueue(new Tuple<string, string>(dir_path, status_message));

                Console.WriteLine(status_message);
            }
        }
    }
}
