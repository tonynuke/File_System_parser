﻿using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Tool.hbm2ddl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemParser.models
{
    public static class FluentNHibernateHelper
    {
        public static ISession OpenSession()
        {
            string connectionString =
                "DATA SOURCE=192.168.1.118:1521/orcl12c;PERSIST SECURITY INFO=True;USER ID=C##MYTESTUSER;PASSWORD=1111;";
                //"DATA SOURCE=192.168.1.118;PASSWORD=1111;PERSIST SECURITY INFO=True;USER ID=C##mytestuser";
                //string.Format("SERVER=(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST={0})(PORT={1}))(CONNECT_DATA=(SID={2})));uid={3};pwd={4};",
                //"192.168.1.118", "1521", "orcl12c", "C##mytestuser", "1111");

            ISessionFactory sessionFactory = Fluently.Configure()
                .Database(OracleClientConfiguration.Oracle10
                .ConnectionString(connectionString).ShowSql())
                .Mappings(m =>
                          m.FluentMappings
                          .AddFromAssemblyOf<FileHash>())

                .ExposeConfiguration(cfg => new SchemaExport(cfg)
                .Create(false, false))
                .BuildSessionFactory();

            return sessionFactory.OpenSession();

            //var cfg = OracleClientConfiguration.Oracle10
            //.ConnectionString(c =>
            //    c.Is(
            //        "DATA SOURCE=192.168.1.118:1521/orcl12c;PERSIST SECURITY INFO=True;USER ID=C##MYTESTUSER;PASSWORD=1111;"));

            //return Fluently.Configure()
            //        .Database(cfg)
            //        .Mappings(m => m.FluentMappings.AddFromAssemblyOf<FileHash>().ExportTo(@".\"))
            //        .ExposeConfiguration(BuildSchema)
            //.BuildSessionFactory();
        }

        //private static void BuildSchema(NHibernate.Cfg.Configuration config)
        //{
        //    // this NHibernate tool takes a configuration (with mapping info in)
        //    // and exports a database schema from it
        //    new SchemaExport(config)
        //      .Create(false, true);
        //}
    }
}
