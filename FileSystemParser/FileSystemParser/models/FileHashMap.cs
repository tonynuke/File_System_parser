﻿using FluentNHibernate.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FileSystemParser.models
{
    class FileHashMap : ClassMap<FileHash>
    {
        public FileHashMap()
        {
            Id(x => x.Id);

            Map(x => x.Name);

            Map(x => x.Hash);

            Map(x => x.Status);

            Table("HASHS");
        }
    }
}
