﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
//using System.Threading.Tasks;

namespace FileSystemParser.models
{
    public class FileHash
    {
        public virtual int Id { get; set; }
        public virtual string Name { get; set; }
        public virtual int Hash { get; set; }
        public virtual string Status { get; set; }

        public FileHash()
        {

        }

        public FileHash(string name, string status="")
        {
            this.Name = name;
            this.Hash = name.GetHashCode();
            this.Status = status;
        }
    }
}
